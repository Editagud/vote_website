<?php
ini_set('display_errors', 0);
$sData = file_get_contents('votes.json');
$jData = json_decode($sData);

if ($jData == null) {sendResponse(0, __LINE__, "Can't get the data");}
$jInnerData = $jData->votes;

$sCPR = $_POST['txtCpr'];

if (empty($sCPR)) {sendResponse(0, __LINE__, 'CPR missing!');}
if (strlen($sCPR) != 8) {sendResponse(0, __LINE__, 'CPR must have 8 characters');}
if (!ctype_digit($sCPR)) {sendResponse(0, __LINE__, 'CPR can contain only digits');}
if ($sCPR == $jInnerData->$sCPR->cpr) {sendResponse(0, __LINE__, "User with this CPR already voted");}



$sVote = $_POST['txtVote'];
//$sVote = 'What';
if (empty($sVote)) {sendResponse(0, __LINE__, 'CPR missing');}

$jInnerData->$sCPR = new stdClass();
$jInnerData->$sCPR->cpr = $sCPR;
$jInnerData->$sCPR->votes = $sVote;

$sData = json_encode( $jData, JSON_PRETTY_PRINT );
file_put_contents('votes.json', $sData);
sendResponse(1, __LINE__, "All good");

function sendResponse($iStatus, $iCode, $sMessage){
    echo '{"status": ' . $iStatus . ', "code":' . $iCode . ', "message": "' . $sMessage . '"}';
    exit;
}
