// global variables //
var selectedCandidate = ''
// global variables //

// submits cpr and vote on click VOTE
$(document).ready(function() {

    $('#btn-vote').click(function(e) {
        submit_vote()
    });

});

// assigns selected candidate to variable
function selectCandidate(candidate)
{
    selectedCandidate = candidate

}

// checks if cpr filled out and candidate selected and runs api
function submit_vote(){

    // checks for missing cpr
    var sCpr = $("#txtCpr").val();
    if (sCpr == null || sCpr == '') {
        swal({title:"WARNING", text: "CPR missing!",   icon: "warning", })
        return
    }

    // checks for missing vote
    if (selectedCandidate == null || selectedCandidate == '') {
        swal({title:"WARNING", text: "No candidate selected!",   icon: "warning", })
        return
    } else {
        var sVote = selectedCandidate
    }

        // if all data filled out runs api
        $.ajax({
            method: "POST",
            url: "api-add-vote.php",
            data: {txtVote: sVote, txtCpr: sCpr},
            dataType:'JSON',
            cache: false
        }).done(function (jData) {
            if(jData.status == 0){
                console.log(jData)
                swal({title:"ERROR", text: jData.message,   icon: "warning", });
                return
            } else {
                location.href = 'thank-you-page'
                return
            }
        }).fail(function () {
            console.log('FATAL ERROR')
        })
        return false

}





