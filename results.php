<?php
ini_set('display_errors', 0);
$sData = file_get_contents('votes.json');
$jData = json_decode($sData, false);

if ($jData == null) {
    sendResponse(0, __LINE__, "Can't get the data");
}
$jInnerData = $jData->votes;
$total = 0;
$jon = 0;
$richard = 0;
$chloe = 0;

foreach ($jInnerData as $sVoterId => $jVoter) {
    $total = $total + 1;
    if ($jVoter->votes == "Jon Walker") {
        $jon = $jon + 1;
    }
    if ($jVoter->votes == "Richard Cooper") {
        $richard = $richard + 1;
    }
    if ($jVoter->votes == "Chloe Barker") {
        $chloe = $chloe + 1;
    }
}




?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>VOTE!</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<header>
    <a href="index.php"> <img src="images/logo.png" id="logo"/></a>
    <div class="header-nav">
        <p class="nav login active">Log in</p>
        <p class="nav">Contact us</p>
        <p class="nav">Candidates</p>
        <p class="nav ">About us</p>
    </div>
</header>
<section align="center">
    <h1 class="title message">Results</h1>
    <p class="result-header">Total votes: <?= $total ?></p>
    <p class="res-cand">Jon Walker: <?= $jon ?></p>
    <p class="res-cand">Richard Cooper: <?= $richard ?></p>
    <p class="res-cand">Chloe Barker: <?= $chloe ?></p>
</section>
</body>
</html>
