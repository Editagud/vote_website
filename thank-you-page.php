<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>VOTE!</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<header>
    <a href="index.php"> <img src="images/logo.png" id="logo"/></a>
    <div class="header-nav">
        <p class="nav login active">Log in</p>
        <p class="nav">Contact us</p>
        <p class="nav">Candidates</p>
        <p class="nav ">About us</p>
    </div>
</header>
<section>
    <h1 class="title message">Thank you for your vote!</h1>
    <h5 align="center"><a href="results.php">View results</a></h5>
</section>
</body>
</html>
