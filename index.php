<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>VOTE!</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
  </head>
  <body>
    <header>
      <a href="index.php"> <img src="images/logo.png" id="logo"/></a>
      <div class="header-nav">
        <p class="nav login active">Log in</p>
        <p class="nav">Contact us</p>
        <p class="nav">Candidates</p>
        <p class="nav ">About us</p>
      </div>
    </header>
    <section>
      <h1 class="title">Vote page</h1>
      <p class="thin">
        Please fill up the form and proceed your voting
      </p>
      <form id="frmVote" method="post">
        <div id="cpr-container">
          <input type="text" class="cpr" name="txtCpr" id="txtCpr" placeholder="Your CPR number" />
        </div>

        <div class="cards-container">
          <div class="card">
            <div class="content">
              <div class="image img-first"></div>
              <div class="radio">
                <input type="radio" name="candidate" id="first" onclick="selectCandidate('Jon Walker')" />
                <span class="candName">Jon Walker</span>
              </div>
            </div>
            <div class="decription">
              Manufacturing is a vital source of good-paying jobs in our
              economy, and making things in Moon is critical to innovation and
              our prosperity. We have to support manufacturers and workers so we
              can compete and win in the global economy.
            </div>
          </div>
          <div class="card">
            <div class="content">
              <div class="image img-second"></div>
              <div class="radio">
                <input type="radio" name="candidate" id="first" onclick="selectCandidate('Richard Cooper')"/>
                <span class="candName">Richard Cooper</span>
              </div>
            </div>
            <div class="decription">
              Climate change is an urgent threat and a defining challenge of our
              time. It threatens our economy, our national security, and our
              children’s health and futures. We can tackle it by making Moon the
              world’s clean energy superpower and creating millions of
              good-paying jobs, taking bold steps to slash carbon pollution at
              home and around the world, and ensuring no moon people are left
              out or left behind as we rapidly build a clean energy economy.
            </div>
          </div>
          <div class="card">
            <div class="content">
              <div class="image img-third"></div>
              <div class="radio">
                <input type="radio" name="candidate" id="first" onclick="selectCandidate('Chloe Barker')" />
                <span class="candName">Chloe Barker</span>
              </div>
            </div>
            <div class="decription">
              I believe that we need an economy that works for everyone, not
              just those at the top. But when it comes to taxes, too often the
              wealthiest and the largest corporations are playing by a different
              set of rules than hardworking families.I am committed to restoring
              basic fairness in our tax code and ensuring that the wealthiest
              the Moon people and large corporations pay their fair share, while
              providing tax relief to working families. That’s not only fair,
              it’s good for economic growth, because she will use the proceeds
              to create good-paying jobs here in Moon—and make bold investments
              that leave our economy more competitive over the long run.
            </div>
          </div>
        </div>

      <div class="button-container">
          <button type="button" id="btn-vote" name="btn-vote">Vote</button>
        </div>
      </form>
    </section>
  </body>
</html>

<script src="add-vote.js"></script>